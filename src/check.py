import pandas as pd

if __name__ == "__main__":
    train_df = pd.read_csv("./data/DevSmallNew.csv")
    print(train_df.describe())
    print(train_df.head())
    print(train_df.columns)
    for col in train_df.columns:
        print(sum(train_df[col].isna()))
    for lab in ['Case Report', 'Diagnosis', 'Epidemic Forecasting', 'Mechanism', 'Prevention', 'Transmission', 'Treatment']:
        print(lab)
        print(sum(train_df[lab] == 'other'))
        print(sum(train_df[lab] == lab))
        print(len(train_df[lab]))
    