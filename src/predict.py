import pandas as pd
import autoBOTLib
import secrets

if __name__ == "__main__":
    jid = "biocreative-7"
    labels = ['Case Report', 'Diagnosis', 'Epidemic Forecasting', 'Mechanism', 'Prevention', 'Transmission', 'Treatment']
    classx = "topic"
    """
    #print(test_data)
    
    test_data['PMID'] = test_data['pmid']
    df = test_data[['PMID', 'Treatment','Diagnosis','Prevention','Mechanism','Transmission','Epidemic Forecasting','Case Report']]
    for col in labels:
        for i in range(len(df[col])):
            if df[col][i] == col:
                df[col][i] = int(1)
            else:
                df[col][i] = int(0)
    df.to_csv("./predictions/gold.csv", index=False)
    """
    test_data = pd.read_csv("./data/test.tsv", sep="\t")
    test_sequences = test_data['text_a'].values.tolist()
    labels = ['Case Report', 'Diagnosis', 'Epidemic Forecasting', 'Mechanism', 'Prevention', 'Transmission', 'Treatment']
    test_data['PMID'] = test_data['pmid']
    for i in range(7):
        #test_data[labels[i]] = pd.Series(0)
        classx = "topic" + str(i)
        autoBOTObj = autoBOTLib.load_autobot_model(
         f"./models/{jid}_{classx}_model.pickle")
        predictions = autoBOTObj.predict_proba(test_sequences)
        #autoBOTObj.generate_report("./report/Report_model_"+str(i))
        print(predictions)
        print(predictions.columns)
        for x in predictions.columns:
            if x != "other":
                test_data[x] = pd.Series(0)
                test_data[x] = predictions[x].values
        
    #print(test_data[["pmid", "label", "labels"]])
    df = test_data[['PMID', 'Treatment','Diagnosis','Prevention','Mechanism','Transmission','Epidemic Forecasting','Case Report']]
    jid = secrets.token_hex(16)
    print(jid)
    df.to_csv(f"./predictions/pred{jid}.csv", index=False)
    

    