import pandas as pd

if __name__ == "__main__":
    df = pd.read_csv("./predictions/gold.csv")
    labels = ['Epidemic Forecasting','Diagnosis','Transmission','Prevention','Treatment','Mechanism','Case Report']
    for x in labels:
        for i in range(len(df)):
            if (df[x].values[i] > 0.5):
                df.loc[i,x] = int(1)
                pd.to_numeric(df.loc[i,x])
            else:
                df.loc[i,x] = int(0)
                pd.to_numeric(df.loc[i,x])
   
    df.to_csv("./predictions/pred.csv", index=False)