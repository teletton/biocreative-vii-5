import autoBOTLib
import pandas as pd
import numpy as np
import secrets
import nltk
#nltk.download('stopwords')
#nltk.download('punkt')
#nltk.download('averaged_perceptron_tagger')
from pandas.io.parsers import read_csv

# Take data frame at path1, 
# add as many columns as are possible labels and for each label write down the label 
# if the list of labels contains that label and store the new data frame to df_path2.
def add_targets(df_path1, df_path2):
    df = pd.read_csv(df_path1, sep="\t")
    df['label'] = df['label'].str.split(";")
    set_unique = set() #set of unique labels
    for l in df['label']:
        for x in l:
            set_unique.add(x)
    for x in set_unique:
        df[x] = pd.Series(dtype="str")
    for x in set_unique:
        for i in range(len(df['label'])):
            if x in df['label'][i]:
                df[x][i] = str(x)
            else:
                df[x][i] = "other"
    #print(df)
    df.to_csv(df_path2, index=False)
    return set_unique
# For each label run autobot
def create_models(target_label, num, jid, df_path):
    train_df = read_csv(df_path, sep=",")

    train_sequences = train_df['text_a'].values.tolist()
    train_targets = train_df[target_label].values.tolist()
    
    for i in range(len(train_targets)):
        if pd.isna(train_targets[i]):
            train_targets[i] = ""
    

    classx = "topic" + str(num)

    autoBOTObj = autoBOTLib.GAlearner(
        train_sequences,
        train_targets,
        time_constraint=1,
        num_cpu=6,
        learner_preset = "knn",
        sparsity=0.1,
        hof_size=3,
        top_k_importances=25,
        representation_type="neurosymbolic")

    autoBOTObj.evolve(
        nind=8,  ## population size
        strategy="evolution",  ## optimization strategy
        crossover_proba=0.6,  ## crossover rate
        mutpb=0.6)  ## mutation rate

    autoBOTLib.store_autobot_model(
        autoBOTObj, f"./models/{jid}_{classx}_model.pickle")

if __name__ == "__main__":
    all_targets = add_targets("./data/train.tsv", "./data/TrainSmallNew.csv")
    all_targets = sorted(all_targets)
    print(all_targets)
    #all_targets = add_targets("./data/test.tsv", "./data/DevSmallNew.csv")
    #all_targets = sorted(all_targets)
    print(all_targets)
    jid = "biocreative-7"

    num = 0 # label number
    for x in all_targets:
        create_models(x, num, jid, "./data/TrainSmallNew.csv")
        num += 1
