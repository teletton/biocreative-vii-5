# BioCreative-VII-5

Run run.sh.
The training is only done without evolution.

For now, I have obtained these results with the evaluation script given by biotech organisers:

| label | precision | recall | f1-score | support |
| ----- | --------- | ------ | -------- | ------- |
| Treatment | 0.8657 | 0.8441 | 0.8548 | 2207 |
| Diagnosis | 0.8917 | 0.7031 | 0.7863 | 1546 |
| Prevention | 0.8935 | 0.9335 | 0.9130 | 2750 |
| Mechanism | 0.8630 | 0.7810 | 0.8200 | 1073 |
| Transmission | 0.7500 | 0.0117 | 0.0231 | 256 |
| Epidemic Forecasting | 0.8780 | 0.1875 | 0.5619 | 192 |
| Case Report | 0.8389 | 0.7884 | 0.8128 | 482 |

|       | precision | recall | f1-score | support |
| ----- | --------- | ------ | -------- | ------- |
| micro avg | 0.8783 | 0.7964 | 0.8353 | 8506 |
| macro avg | 0.8544  |  0.6070  |  0.6456 | 8506 |
| weighted avg | 0.8744 | 0.7964 | 0.8170 | 8506 |
| samples avg | 0.8559 | 0.8353 | 0.8290 | 8506 |

## Instance-based measures
|      |        |       
| ---- | ------ | 
| mean precision | 0.8559 |
| mean recall | 0.8353 |
| f1 | 0.8455 |

## Second results

| label | precision  |   recall | f1-score  |  support |
| ---------- | --------- | ------ | ------- | ------- | 
| Treatment  |   0.8751   | 0.8351  |  0.8546  |    2207 |
| Diagnosis   |  0.8560   | 0.7652  |  0.8081   |   1546 |
| Prevention  |   0.9120  |  0.9196  |  0.9158  |    2750 |
| Mechanism   |  0.8600   | 0.7903  |  0.8237    |  1073 |
| Transmission  |   0.8333  |  0.0195  |  0.0382  |     256 |
| Epidemic Forecasting   |  0.8833   | 0.2760  |  0.4206 | 192 |
| Case Report  |   0.8811  |  0.7842  |  0.8299   |    482 |

|       | precision | recall | f1-score | support |
| ----- | --------- | ------ | -------- | ------- |
| micro avg   |  0.8834 |   0.8040  |  0.8418   |   8506 |
| macro avg   |  0.8716  |  0.6271  |  0.6701   |   8506 |
| weighted avg  |   0.8809  |  0.8040  |  0.8263   |   8506 |
| samples avg |     0.8637  |  0.8428  |  0.8364   |   8506 |

|      |        |       
| ---- | ------ | 
| instance-based | measures |
| mean precision | 0.8637 |
| mean recall | 0.8428 |
| f1  | 0.8531 |
